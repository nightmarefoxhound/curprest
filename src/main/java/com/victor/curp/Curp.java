package com.victor.curp;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.victor.entidades.CurpEntity;

@Service
public class Curp {

	/**
	 * Variable apellido paterno
	 */
	private String paterno;

	/**
	 * Variable apellido materno
	 */
	private String materno;

	/**
	 * Variable nombre
	 */
	private String nombre;

	/**
	 * Variable que almacena el conjunto de 4 palabras y fecha
	 */
	private String Pos10;

	/**
	 * Variable fecha
	 */
	private String wFecNac;

	/**
	 * Array de vocales
	 */
	private String[] vocales;

	/**
	 * Array de palabras no permitidas
	 */
	private String[] noPerm;

	/**
	 * Array de palabras ofensivas
	 */
	private String[] ofensivas;

	/**
	 * Metodo que genera el CURP y puede ser llamado para realizar pruebas.
	 * 
	 * @param aPaterno    apellido paterno
	 * @param aMaterno    apellido materno
	 * @param nombres     nombres
	 * @param fecha       fecha nacimiento
	 * @param sexo        sexo de la persona
	 * @param lNacimiento lugar nacimiento
	 * @return metodo que genera el CURP
	 */
	public List<String> generaCurp(String aPaterno, String aMaterno, String nombres, String fecha, String sexo,
			String lNacimiento) {
		
		String curp;
		String digito1;
		String vSexo = "";
		
		CurpEntity curpe = new CurpEntity();
		List<String> curpL = null;
		
		try {
		

		// SEGMENTO DE CODIGO QUE INICIALIZA LAS MATRICES QUE SE UTILIZAN DURANTE EL
		// PROCESO
		inicializa();

		if (aPaterno.length() == 0 && aMaterno.length() == 0) {
			throw new NullPointerException("Sin Apellidos");
			//return " SIN APELLIDOS ";
		}
		if (nombres.length() == 0) {
			throw new NullPointerException("Sin Nombre");
			//return " SIN NOMBRE ";
		}
		if (fecha.length() == 0) {
			throw new NullPointerException("Sin Fecha");
			//return " SIN FECHA ";
		}
		

		boolean finice;

		aPaterno = aPaterno.toUpperCase();
		aMaterno = aMaterno.toUpperCase();
		nombres = nombres.toUpperCase();

		aPaterno = aPaterno.substring(0, 1).equals("Ñ") ? aPaterno.substring(0).replace("Ñ", "X") : aPaterno;
		aMaterno = aMaterno.substring(0, 1).equals("Ñ") ? aMaterno.substring(0).replace("Ñ", "X") : aMaterno;
		nombres = nombres.substring(0, 1).equals("Ñ") ? nombres.substring(0).replace("Ñ", "X") : nombres;

		Pos10 = "";

		// Elimina espacios al principio y final de la cadena
		paterno = aPaterno.trim();
		materno = aMaterno.trim();
		nombre = nombres.trim();

		// Convierte fecha nacimiento a formato solicitado
		wFecNac = fecha.substring(8, 10) + fecha.substring(3, 5) + fecha.substring(0, 2);

		finice = false;

		/**
		 * SEGMENTO DE CODIGO QUE SE ENCARGA DE ELIMINAR CONJUNCIONES O ARTICULOS
		 * PRESENTES EN LOS ELEMENTOS QUE INTEGRAN EL NOMBRE DEL CLIENTE
		 */
		paterno = octava(paterno);
		materno = octava(materno);
		nombre = octava(nombre);

		paterno = paterno.trim();
		materno = materno.trim();
		nombre = nombre.trim();

		// Culmina el proceso si no es posible generar la clave por falta de datos
		if (paterno.length() == 0 || materno.length() == 0) {
			septima();
			finice = true;
		}

		if (!finice) {
			obtieneSegmento();
		}

		Pos10 = Pos10.substring(0, 4) + Pos10.substring(4, 10);
		// SI EL AÑO DE NACIMIENTO ES MENOR AL AÑO 2000 SE ASIGNA COMO HOMOCLAVE EL 0
		// (CERO)
		// SI EL AÑO DE NACIMIENTO ES MAYOR O IGUAL AL AÑO 2000 SE ASIGNA COMO HOMOCLAVE
		// LA LETRA A

		String anio = fecha.substring(6, 10);
		int year = Integer.parseInt(anio);

		if (year < 2000) {
			digito1 = "0";
		} else {
			digito1 = "A";
		}

		char[] sex = sexo.toCharArray();
		char genero = sex[0];

		// ' El sexo es H Hombre, M Mujer
		switch (genero) {
		case 'H':
			vSexo = "H";
			break;
		case 'M':
			vSexo = "M";
			break;
		case 'U':
			vSexo = "U";
			break;
		default:
			System.out.println("Error");
		}

		curp = Pos10 + vSexo + lNacimiento + extraeConsonantes(paterno, materno, nombre) + digito1;
		curp = digitoVerificador(curp);
		
		curpL = new ArrayList<String>(Arrays.asList(curp));
		
		
		} catch(Exception e) {
			e.getMessage();
		}
		return curpL;
	}

	/**
	 * Método que almacena palabras no permitidas u ofensivas.
	 */
	private void inicializa() {
		String cadena;
		cadena = "A,E,I,O,U";
		vocales = cadena.split(",");

		cadena = "DE ,DEL ,LA ,LOS ,LAS ,Y ,MC ,MAC ,VON ,VAN ,JOSE ,MARIA ,J ,MA ,J. ,M. ,MA.";
		noPerm = cadena.split(",");

		cadena = "BACA,BAKA,BUEI,BUEY,CACA,CACO,CAGA,CAGO,CAKA,CAKO," + "COGE,COGI,COJA,COJE,COJI,COJO,COLA,CULO,"
				+ "FALO,FETO,GETA,GUEI,GUEY,JETA,JOTO,KACA," + "KACO,KAGA,KAGO,KAKA,KAKO,KOGE,KOGI,KOJA,"
				+ "KOJO,KOJE,KOJI,KOJO,KOLA,KULO,LILO,LOCA," + "LOCO,LOKA,LOKO,MAME,MAMO,MEAR,MEAS,MEON,"
				+ "MIAR,MION,MOCO,MOKO,MULA,MULO,NACA,NACO," + "PEDA,PEDO,PENE,PIPI,PITO,POPO,PUTA,PUTO,"
				+ "QULO,RATA,ROBA,ROBE,ROBO,RUIN,SENO,TETA," + "VACA,VAGA,VAGO,VAKA,VUEI,VUEY,WUEI,WUEY";
		ofensivas = cadena.split(",");

	}

	/**
	 * Metodo encargado de mostrar las palabras prohibidas y sustituirlas con una X.
	 */
	public void muestra() {
		long van;
		van = palabrasIncorrectas(ofensivas, Pos10.substring(0, 4));
		if (van > 0) {
			Pos10 = Pos10.substring(0, 1) + "X" + Pos10.substring(2);
		}
	}

	/**
	 * 
	 * Función que concatena las primeras 4 letras de la clave con la cadena de la
	 * fecha
	 */
	public void obtieneSegmento() {
		String letra = "X";
		int x;
		long van;
		for (x = 1; x < paterno.length(); x++) {
			van = palabrasIncorrectas(vocales, Character.toString(paterno.charAt(x)));
			if (van >= 0) {
				// Segmento que se ocupa para integrar la primer
				// vocal presente en el apellido paterno
				letra = vocales[(int) van];
				x = paterno.length();
			} else {
				letra = "X";
			}
		}
		Pos10 = paterno.substring(0, 1) + letra + materno.substring(0, 1) + nombre.substring(0, 1);
		Pos10 = Pos10 + wFecNac;
		muestra();
	}

	/**
	 * Segmento que indica la imposibilidad de generar la clave por la ausencia de
	 * datos
	 */
	private void septima() {
		String unoSolo;
		if (paterno.length() == 0 && materno.length() > 0) {
			unoSolo = materno;
		} else {
			if (paterno.length() > 0 && materno.length() == 0) {
				unoSolo = paterno;
			} else {
				unoSolo = nombre;
			}
		}
		Pos10 = unoSolo.substring(0, 2) + nombre.substring(0, 2) + wFecNac + "000";
		muestra();
	}

	/**
	 * Función que elimina de los apellidos las palabras (del, la, los) así como de
	 * los nombres las palabras (Maria, Jose) basandose en la variable noPerm que
	 * contiene este tipo denominaciones
	 * 
	 * @param vNomApe variable que verifica Nombre o Apellidos
	 * @return metodo para palabras no permitidas
	 */
	private String octava(String vNomApe) {
		long i;
		int ind;
		for (i = 0; i < noPerm.length; i++) {
			ind = vNomApe.indexOf(noPerm[(int) i]);
			if (ind >= 0) {
				if ((ind - 1) < 0 || Character.toString(vNomApe.charAt(ind - 1)).equals(" ")) {
					vNomApe = vNomApe.replace(noPerm[(int) i], "");
				}
			}
		}
		return vNomApe;
	}

	/**
	 * Función que reemplaza las palabras altisonantes dentro del primer segmento de
	 * la clave
	 * 
	 * @param vMatriz checa la matriz de palabras
	 * @param vValor  obtiene el valor que se busca
	 * @return verifica que no haya palabras mal escritas
	 */
	public int palabrasIncorrectas(final String[] vMatriz, final String vValor) {
		int i;
		Boolean vStop;
		vStop = false;
		for (i = 0; (i < vMatriz.length && !vStop); i++) {
			if (vMatriz[i].equals(vValor)) {
				vStop = true;
				break;
			}
		}
		if (vStop) {
			return i;
		} else {
			return -1;
		}
	}

	/**
	 * Metodo encargado de verificar consonantes.
	 * 
	 * @param aPaterno apellido paterno
	 * @param aMaterno apellido materno
	 * @param nombres  nombres
	 * @return busca las consonantes del cliente
	 */
	private String extraeConsonantes(String aPaterno, String aMaterno, String nombres) {
		String vocal = "AEIOU";
		String consonantes;
		int i;
		Boolean bandCons = false;
		consonantes = "";
		// Busca la primer consonante del apellido paterno
		for (i = 1; i < aPaterno.length(); i++) {
			if (vocal.indexOf(Character.toString(aPaterno.charAt(i)), 0) < 0) {
				consonantes = Character.toString(aPaterno.charAt(i));
				bandCons = true;
				break;
			}
		}
		// Incorpora una X si no encontro más consonantes en el
		// apellido paterno
		if (!bandCons) {
			consonantes = "X";
		}

		bandCons = false;
		// Busca la primer consonante del apellido materno
		for (i = 1; i < aMaterno.length(); i++) {
			if (vocal.indexOf(Character.toString(aMaterno.charAt(i)), 0) < 0) {
				consonantes = consonantes + Character.toString(aMaterno.charAt(i));
				bandCons = true;
				break;
			}
		}

		// Incorpora una X si no encontro mas consonantes en el
		// apellido materno
		if (!bandCons) {
			consonantes = consonantes + "X";
		}

		bandCons = false;
		for (i = 1; i < nombres.length(); i++) {
			if (vocal.indexOf(Character.toString(nombres.charAt(i)), 0) < 0) {
				consonantes = consonantes + Character.toString(nombres.charAt(i));
				bandCons = true;
				break;
			}
		}
		// Incorpora una X si no encontro mas consonantes en el nombre
		if (!bandCons) {
			consonantes = consonantes + "X";
		}
		return consonantes.replace("Ñ", "X");
	}

	/**
	 * Función que genera el digito verificador al final de la clave
	 * 
	 * @param curp curp generado
	 * @return genera el digito verificador del curp
	 */
	public String digitoVerificador(String curp) {
		String segRaiz = curp.substring(0, curp.length());
		String chrCaracter = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
		int[] intFactor; // declarar el array 09
		intFactor = new int[17];

		long lngSuma = 0;
		long lngDigito = 0;

		for (int i = 0; i < curp.length(); i++) {
			for (int j = 0; j < 37; j++) {
				int pos = chrCaracter.indexOf(Character.toString(segRaiz.charAt(i)));
				intFactor[i] = pos;
			}
		}

		for (int k = 0; k < curp.length(); k++) {
			lngSuma = lngSuma + ((intFactor[k]) * (18 - k));
		}

		lngDigito = (10 - (lngSuma % 10));

		if (lngDigito == 10) {
			lngDigito = 0;
		}

		curp = curp + lngDigito;
		return curp;
	}

}
