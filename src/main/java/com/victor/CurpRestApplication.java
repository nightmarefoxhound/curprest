package com.victor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurpRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurpRestApplication.class, args);
	}

}
