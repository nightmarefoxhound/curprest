package com.victor.entidades;

public class CurpEntity {

	/**
	 * Variable apellido paterno
	 */
	private String paterno;

	/**
	 * Variable apellido materno
	 */
	private String materno;

	/**
	 * Variable nombre
	 */
	private String nombre;

	/**
	 * Variable fecha
	 */
	private String fecNac;

	/**
	 * Variable sexo
	 */
	private String sexo;

	/**
	 * Variable lugar nacimiento
	 */
	private String lugarNac;
	
	private String curpCompleto;

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFecNac() {
		return fecNac;
	}

	public void setFecNac(String fecNac) {
		this.fecNac = fecNac;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getLugarNac() {
		return lugarNac;
	}

	public void setLugarNac(String lugarNac) {
		this.lugarNac = lugarNac;
	}

	public String getCurpCompleto() {
		return curpCompleto;
	}

	public void setCurpCompleto(String curpCompleto) {
		this.curpCompleto = curpCompleto;
	}
	
	

}
