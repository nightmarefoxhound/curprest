package com.victor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.victor.curp.Curp;
import com.victor.entidades.CurpEntity;

@RestController
@RequestMapping("curp")
public class CurpController {

	@Autowired
	private Curp curp;

	@GetMapping("/obtener")
	public List<String> obtener(@RequestBody CurpEntity curpE){
		List<String> curpCreado = curp.generaCurp(curpE.getPaterno(), curpE.getMaterno(), curpE.getNombre(), 
				curpE.getFecNac(), curpE.getSexo(), curpE.getLugarNac());
		return curpCreado;
	}

}
